-- from http://haskell.org/haskellwiki/Xmonad/Config_archive/Template_xmonad.hs_(0.8)

import XMonad
import System.Exit
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName
import XMonad.Layout
import XMonad.Layout.Grid
import XMonad.Layout.AutoMaster
import XMonad.Layout.Tabbed
import XMonad.Layout.BorderResize
import XMonad.Layout.SimpleFloat
import XMonad.Layout.NoBorders
import XMonad.Layout.Magnifier
import System.IO

import qualified XMonad.StackSet as W
import qualified Data.Map as M

-- we can get the class name with xprop
myManageHook = composeAll
    [ className =? "Gimp"      --> doFloat
    , className =? "Vncviewer" --> doFloat
    , className =? "VirtualBox" --> doFloat
    , title =? "qiv" --> doFloat
    , className =? "Firefox" --> doShift "1"
    , (className =? "Firefox" <&&> resource =? "Dialog") --> doFloat
    , (className =? "Firefox" <&&> title =? "Téléchargements") --> doFloat
    , className =? "Firefox-esr" --> doShift "1"
    , (className =? "Firefox-esr" <&&> resource =? "Dialog") --> doFloat
    , (className =? "Firefox-esr" <&&> title =? "Téléchargements") --> doFloat
    , (className =? "Firefox-esr" <&&> title =? "promptPassword") --> doFloat
    , (className =? "Thunderbird" <&&> resource =? "Dialog") --> doFloat
    , className =? "Pidgin" --> doShift "12"
    , className =? "rdesktop" --> doShift "11"
    , className =? "linphone" --> doShift "11"
    , className =? "thunderbird-default" --> doShift "0"
    , className =? "linphone" --> doFloat
    , className =? "gnome-calculator" --> doFloat
    , className =? "grdesktop" --> doShift "11"
    ]

startup :: X ()
startup = do
    spawn "start_session.sh"
    spawn "xloadimage -onroot -fullscreen ~/Images/desktop_1920x1200.png"
    spawn "dontlaunch=0; for i in $(pidof python) ; do grep -q pidgin_evt_dump.py /proc/$i/cmdline && dontlaunch=1; done; test $dontlaunch -eq 0 && pidgin_evt_dump.py | xmobar /home/$USER/.xmonad/xmobar-bottom"
    spawn "dontlaunch=0; for i in $(pidof sh) ; do grep -a my_rss.sh /proc/$i/cmdline | grep -a -q -v grep && dontlaunch=1; done; test $dontlaunch -eq 0 && my_rss.sh | xmobar /home/$USER/.xmonad/xmobar-bottom2"
-- to test : runOrRaise "chromium" (className =? "Chromium")
-- not anymore on jessie :
--	spawn "pidof update-notifier || update-notifier"
--	spawn "pidof kerneloops-applet || kerneloops-applet"

myWorkspaces = map show [0 .. 12 :: Int]
 
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    -- launch a terminal
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
    -- launch dmenu
    , ((modm,               xK_p     ), spawn "exe=`dmenu_path | dmenu` && eval \"exec $exe\"")
    -- launch gmrun
    , ((modm .|. shiftMask, xK_p     ), spawn "gmrun")
    -- close focused window
    -- , ((modm .|. shiftMask, xK_c     ), kill) -- default
    , ((modm, xK_F4     ), kill)
    , ((modm .|. shiftMask, xK_F4     ), spawn "kill -9 firefox-esr")
     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)
    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)
    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)
    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)
    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )
    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )
    -- Swap the focused window and the master window
    , ((modm,               xK_Return), windows W.swapMaster)
    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )
    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )
    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)
    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)
    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)
    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))
    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))
    -- toggle the status bar gap (used with avoidStruts from Hooks.ManageDocks)
    -- , ((modm , xK_b ), sendMessage ToggleStruts)
    -- Quit xmonad
    , ((modm .|. shiftMask, xK_x     ), io (exitWith ExitSuccess))
    -- Restart xmonad
    , ((modm              , xK_x     ), restart "xmonad" True)
    -- Start missing session sofwares
    , ((modm              , xK_q     ), spawn "start_session.sh")
    -- Kill pidgin notifications
    , ((modm .|. shiftMask, xK_q     ), spawn "kill_pidgin_notifications.sh")
    ]
    ++
    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
    ++
    -- My configuration
    [
    -- launch a terminal
    ((modm, xK_t), spawn $ XMonad.terminal conf)
    -- switch to US layout keyboard
    , ((modm .|. shiftMask, xK_e), spawn "setxkbmap -layout us")
    -- switch to FR layout keyboard
    , ((modm .|. shiftMask, xK_f), spawn "setxkbmap -layout fr -variant oss")
    -- master volume up
    , ((modm, xK_KP_Add), spawn "amixer -c2 sset Speaker 5%+")
    -- master volume down
    , ((modm, xK_KP_Subtract), spawn "amixer -c2 sset Speaker 5%-")
    -- sound in a sane state (stored with /usr/sbin/alsactl -f ~/etc/asound.state store)
    , ((modm, xK_KP_Multiply), spawn "/usr/sbin/alsactl -f ~/etc/asound.state restore")
    -- lock screen
    , ((controlMask .|. shiftMask, xK_l), spawn "gnome-screensaver-command -l")
    , ((modm .|. shiftMask, xK_l), spawn "gnome-screensaver-command -l")
    -- suspend and lock screen
    , ((controlMask .|. shiftMask .|. modm, xK_l), spawn "xautolock -locknow")
    -- floating layer support
    , ((modm .|. shiftMask, xK_t), withFocused $ windows . W.sink) -- %! Push window back into tiling
    -- launch proxied chromium
    , ((shiftMask .|. modm, xK_w), spawn "chromium --proxy-server=\"socks5://127.0.0.1:8080\"")
    -- launch incognito chromium
    , ((controlMask .|. modm, xK_w), spawn "chromium --incognito")
    -- Print screen (select area)
    , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
    -- Print screen (selected window)
    , ((shiftMask , xK_Print), spawn "scrot -u")
    -- Print screen
    , ((0, xK_Print), spawn "scrot")
    -- To make java app work
    , ((modm, xK_j), setWMName "LG3D")
    ]
    ++
    -- switch workspaces azerty
    -- mod-[²..=] @@ Switch to workspace N
    -- mod-shift-[²..=] @@ Move client to workspace N
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf)
            [ xK_twosuperior, xK_ampersand, xK_eacute, xK_quotedbl, xK_apostrophe, xK_parenleft, xK_minus
             , xK_egrave, xK_underscore, xK_ccedilla, xK_agrave, xK_parenright, xK_equal ]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
    ]
-- (Tall 1 (3/100) (1/2)) == tiled
-- mylayoutHook = avoidStruts  $ smartBorders(Tall 1 (3/100) (1/2)) ||| smartBorders(Mirror (Tall 1 (3/100) (1/2))) ||| smartBorders(autoMaster 1 (1/100) Grid) ||| smartBorders(Mirror (autoMaster 1 (1/100) Grid)) ||| Grid ||| Full ||| simpleTabbed ||| smartBorders(withBorder 3 tiled) ||| smartBorders(withBorder 3 (Mirror tiled))
-- mylayoutHook = avoidStruts  $ smartBorders(Mirror (autoMaster 1 (1/100) Grid)) ||| smartBorders(Mirror (Tall 1 (3/100) (1/2))) ||| Grid ||| Full
mylayoutHook = avoidStruts  $  smartBorders(Tall 1 (3/100) (1/2)) ||| smartBorders(Mirror (Tall 1 (3/100) (1/2))) ||| Grid ||| Full

main = do
    xmproc <- spawnPipe "xmobar /home/$USER/.xmonad/xmobarrc"
    xmonad $ ewmh def -- ewmh prevents the libreoffice bug on dialogs (focus flickering)
        { manageHook = manageDocks <+> myManageHook -- make sure to include myManageHook definition from above
                        <+> manageHook def
        , layoutHook = mylayoutHook
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        -- prevent xmobars from being covered
        , handleEventHook = mconcat
                          [ docksEventHook
                          , handleEventHook def ]
        -- , modMask = mod4Mask     -- Rebind Mod to the Windows key
        , terminal = "mate-terminal" -- "urxvtc -tr -sh 30"
        , workspaces = myWorkspaces
        , startupHook = startup

        , keys = myKeys
        }
